## General:
A bash script useful for fast switching between different Mozilla Firefox versions.
Uses native 'update-alternatives' for symlink handling.

Works on Ubuntu-based distributions, should work on any debian distro.

## Features:

- download custom version from mozilla web-ftp site;
- maintain different versions of browser locally;
- display currently installed firefox binary and its version;

## Depends:

sudo apt-get install xmllint

## Usage:

(optional) create symlink to user bin folder:
```
sudo ln -s /path/to/script/firefox_version.sh /usr/bin/firefox_version
```

```
  firefox_version -v                   for print available versions;
  firefox_version --install <version>  will install needed version to /opt/firefoxes (adjust if needed in script header).
```