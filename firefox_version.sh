#!/bin/bash

INSTALLPATH=/opt/firefoxes

VERSION_MASK='[3-9]\d[0-9.]+' # limit ftp versions starting from 30ties..


usage() {
  echo "Invalid argument: version should be of form '45.0.5'."
  echo "Usage:
  $(basename $0) -v                   for print available versions;
  $(basename $0) --install <version>  will install needed version to $INSTALLPATH;"
}

check_firefox_running() {
    echo -e "Checking if there are running firefox instances..\n"
    if ps -o pid,cmd -C "firefox" &>/dev/null; then
        echo "Found running instance(s)."
        ps -o pid,cmd -C "firefox"
        echo -e "\nTerminate all of them and restart script."
        exit 1
    fi
}

get_ftp_versions_by_mask() {
    curl -s https://ftp.mozilla.org/pub/firefox/releases/ | 
    xmllint --html --xpath "/html/body/table/tr/td/a" - | 
    grep -oP "(?<=\">)$1?(?=\/<\/a>)" 
}

get_installed_versions() {
    find "$INSTALLPATH" -maxdepth 1 -mindepth 1 -type d -printf "%f\n"
    #include system default installation
    get_version_by_path '/usr/lib/firefox/firefox' | sed 's/Version=//'
}

install_version() {
    [[ $(getconf LONG_BIT) = 64 ]] && ARCH="x86_64" || ARCH="i686"

    cd "$INSTALLPATH" &&
    wget "https://ftp.mozilla.org/pub/firefox/releases/$1/linux-$ARCH/en-US/firefox-$1.tar.bz2" &&
    mkdir "$INSTALLPATH/$1" && cd "$_" &&
    tar xf "$INSTALLPATH/firefox-$1.tar.bz2"
}

check_installation() {
    find "$INSTALLPATH/$1" -type f -name firefox | egrep '.*'
}

update_alts() {
    
        sudo update-alternatives --install /usr/bin/firefox firefox $(check_installation "$1") -5 &>/dev/null &&
        sudo update-alternatives --set firefox $(check_installation "$1")
}

get_active_link() {
    update-alternatives --query firefox | grep -oP 'Value:\s\K.*' | tr -d '\n'
}

get_version_by_path() {
    grep -e '^Version' $(dirname $1)/application.ini 2>/dev/null
}

# parse command-line args
if [[ "$1" == "-v" ]]; then
   
   echo -e "Currently active executable:"
   echo $(get_active_link)         [ $(get_version_by_path `get_active_link`) ]

   echo -e "\nInstalled locally:"
   get_installed_versions | tr '\n' ' ' && echo ''
   echo -e "\nAvailable on Mozilla web ftp:"
   get_ftp_versions_by_mask $VERSION_MASK | tr '\n' ' ' && echo ''

elif [[ "$1" == "--install" ]]; then

   # check if install dir exists, if not - create it or exit with error
   test -d "$INSTALLPATH" || mkdir -p "$INSTALLPATH" || { echo "Could not create install dir: $INSTALLPATH. Check rights or change it in script body." && exit 1; }

   check_firefox_running;
   if check_installation "$2" &> /dev/null; then
       echo 'Version already installed locally. Updating links.'
   elif get_ftp_versions_by_mask $VERSION_MASK | grep "$2" &> /dev/null; then
       install_version "$2" && check_installation "$2" && echo "OK: Installed $2 to $INSTALLPATH."
   else
    echo "This version is not present neither locally, nor on Mozilla FTP."
    exit 1
   fi

   update_alts "$2" || echo -e "\nUpdating links failed for $2. Check dir structure $INSTALLPATH/$2/"

else

   usage
   exit 1

fi
